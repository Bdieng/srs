import { Component } from '@angular/core';
import {Personnes} from './Personne';
import {createFile} from './Personne';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'Système automatisé d\'enregistrement des étudiants';
  inscription = 'S\'inscrire';
  exporter = 'Exporter la liste des étudiants';
  listCours = 'Voir la liste des cours';
  personnes = Personnes;
  isExported = false;
  exportList() {
    if (Personnes.length) {
      createFile('etudiants.csv');
      this.isExported = true;
    }
  }
}



