import { saveAs } from 'file-saver';
import {Type} from './Type';
export class Personne {

  firstname: string;
  lastname: string;
  status: Type;
  specialite: string;
  niveau: string;

  constructor(firstname: string, lastname: string, status: Type, specialite: string, niveau: string) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.status = status;
    this.specialite = specialite;
    this.niveau = niveau;
  }

  toString = () => {
    return this.firstname + ';' + this.lastname + ';' + this.status + ';' + this.specialite + this.niveau;
  }
}

export let Personnes: Personne [] = [];

export function createFile(filename: string) {
  let text = '';
  Personnes.forEach((element) => {
    text += element.toString() + '\n';
  });
  const blob = new Blob([text], {type: 'text/plain;charset=utf-8'});
  saveAs(blob, filename);
}
