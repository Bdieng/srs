import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Personne} from '../Personne';
import {Personnes} from '../Personne';
import {Type} from '../Type';


@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {
  inscription = 'S\'inscrire';
  close = 'Annuler';
  file: File;
  diplomes = ['Licence 1',
    'Licence 2',
    'Licence 3',
    'Master 1',
    'Master 2'];
  majeures = ['Génie Informatique', 'Génie Electrique', 'Systéme et Réseau', 'Biologie', 'Bio-Chimie', 'Autre'];
  diplome = new FormControl('');
  majeure = new FormControl('');
  prenom = new FormControl('');
  nom = new FormControl('');
  onClick() {
    Personnes.push(new Personne(this.prenom.value, this.nom.value, Type.Etudiant, this.majeure.value, this.diplome.value));
  }
}
