import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoraireCoursComponent } from './horaire-cours.component';

describe('HoraireCoursComponent', () => {
  let component: HoraireCoursComponent;
  let fixture: ComponentFixture<HoraireCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoraireCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoraireCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
