import { Component} from '@angular/core';
import { Classes } from '../Classe';

@Component({
  selector: 'app-horaire-cours',
  templateUrl: './horaire-cours.component.html',
  styleUrls: ['./horaire-cours.component.css']
})
export class HoraireCoursComponent {
  classes = Classes;

}
