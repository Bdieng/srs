import {Personne} from './Personne';
import {Type} from './Type';

enum Semaine {
  S1 = 'S1', S2 = 'S2', S3 = 'S3', S4 = 'S4', S5 = 'S5', S6 = 'S6', S7 = 'S7', S8 = 'S8'
}

export interface Classe {
  name: string;
  professeur: Personne;
  notemin: number;
  horaires: string[];
  semaine: Semaine;

}

export const Classes = [
  {
    name: 'Java',
    professeur: new Personne('P1', 'P1', Type.Professeur, '', ''),
    notemin: 10,
    horaires: ['10h-12h'],
    semaine: Semaine.S1
  },
  {
    name: 'C++',
    professeur: new Personne('P2', 'P2', Type.Professeur, '', ''),
    notemin: 10,
    horaires: ['9h-10h'],
    semaine: Semaine.S1
  },
];
